using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using TestProject.Models;

namespace TestProject.Controllers
{
    [Route("api/v1/person/{PersonId}/phone")]
    [ApiController]
    public class PhoneController : ControllerBase
    {
        private readonly CrudAppContext _context;

        public PhoneController(CrudAppContext context)
        {
            _context = context;
        }

        // GET api/v1/person/{person_id}/phone
        [HttpGet(Name = "IndexPhone")]
        public ActionResult <List<Phone>> Get(long PersonId)
        {
            Person person = _context.Persons.Find(PersonId);

            if( person == null )
                return NotFound();

            List<Phone> phones = _context.Phones
                                         .Where( p => p.PersonID == PersonId)
                                         .ToList();


            return phones;
        }

        // GET api/v1/person/{person_id}/phone/{phone_id}
        [HttpGet("{PhoneId}", Name = "GetPhone")]
        public ActionResult<Phone> GetById(long PersonId, long PhoneId)
        {
            Person person = _context.Persons.Find(PersonId);

            if ( person == null )
                return NotFound();

            Phone phone = _context.Phones.Find(PhoneId);

            return phone;
        }

        // POST api/v1/person/{person_id}/phone
        [HttpPost(Name = "CreatePhone")]
        public IActionResult Create(long PersonId, Phone phone)
        {
            Person person = _context.Persons.Find( PersonId );  

            if( person == null )
                return NotFound();

            phone.PersonID = person.Id;

            _context.Phones.Add( phone );
            _context.SaveChanges();

            return CreatedAtRoute("GetPhone", routeValues: new {PhoneId = phone.Id}, value: new {phone = phone});
        }

        // PUT  api/v1/person/{person_id}/phone/{phone_id}
        [HttpPut("{PhoneId}", Name = "UpdatePhone")]
        public IActionResult Update(long PersonId, long PhoneId, Phone SentPhone)
        {
            Person person = _context.Persons.Find( PersonId );

            if ( person == null )
                return NotFound();

            Phone phone = _context.Phones.Find( PhoneId );

            if( phone == null )
                return NotFound();

            phone.Numero = SentPhone.Numero;
            phone.Ddd    = SentPhone.Ddd;

            _context.Phones.Update( phone );
            _context.SaveChanges();

            return NoContent();
        }

        // DELETE api/v1/person/{person_id}/phone/{phone_id}
        [HttpDelete("{PhoneId}", Name = "DeletePhone")]
        public IActionResult Delete( long PersonId, long PhoneId)
        {
            Person person = _context.Persons.Find( PersonId );
            if( person == null )
                return NotFound();

            Phone phone = _context.Phones.Find(PhoneId);
            if( phone == null )
                return NotFound();

            _context.Phones.Remove( phone );
            _context.SaveChanges();

            return NoContent();
        }
    }
}