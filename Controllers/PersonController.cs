﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using TestProject.Models;

namespace TestProject.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly CrudAppContext _context;

        public PersonController (CrudAppContext context)
        {
            _context = context;
        }

        // GET api/v1/person
        [HttpGet(Name = "IndexPerson")]
        public ActionResult<List<Person>> Get()
        {
            List<Person> people = new List<Person>();
            foreach(Person person in _context.Persons.ToList())
            {
                person.Phones = _context.Phones
                                        .Where( p => p.PersonID == person.Id)
                                        .ToList();
                people.Add(person);
            }

            return people;
        }

        // GET api/v1/person/{id}
        [HttpGet("{id}", Name = "GetPerson")]
        public ActionResult<Person> GetById(long Id)
        {
            Person person = _context.Persons.Find(Id);

            if( person == null )
                return NotFound();

            person.Phones = _context.Phones
                                    .Where( p => p.PersonID == Id )
                                    .ToList();

            return person;
        }

        // POST api/v1/person
        [HttpPost(Name = "CreatePerson")]
        public IActionResult Create(Person person)
        {
            _context.Persons.Add(person);
            _context.SaveChanges();

            return CreatedAtRoute("GetPerson", new { id = person.Id}, person);
        }

        // PUT api/v1/person/{id}
        [HttpPut("{id}", Name = "UpdatePerson")]
        public IActionResult Update(long id, Person sent_person)
        {
            var person = _context.Persons.Find( id );
            if( person == null )
                return NotFound();

            person.Name  = sent_person.Name;
            person.Cpf   = sent_person.Cpf;
            person.Email = sent_person.Email;
            person.DataNascimento = sent_person.DataNascimento;

            _context.Persons.Update( person );
            _context.SaveChanges();

            return NoContent();
        }

        // DELETE api/v1/person/{id}
        [HttpDelete("{id}", Name = "DeletePerson")]
        public IActionResult Delete( long id )
        {
            var person = _context.Persons.Find( id );

            if ( person == null )
            {
                return NotFound("Person don't exist!");
            }

            _context.Persons.Remove( person );
            _context.SaveChanges();

            return NoContent();
        }
    }
}
