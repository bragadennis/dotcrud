namespace TestProject.Models
{
    public class Phone
    {
        public long Id { get; set; }
        public long PersonID { get; set; }
        public string Ddd { get; set; }
        public string Numero { get; set; }
    }
}