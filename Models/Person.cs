using System;
using System.Collections.Generic;

namespace TestProject.Models
{
    public class Person
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Cpf { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Email { get; set; }
        public List<Phone> Phones { get; set; }
    }
}