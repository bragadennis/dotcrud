using Microsoft.EntityFrameworkCore;

namespace TestProject.Models
{
    public class CrudAppContext : DbContext
    {
        public CrudAppContext (DbContextOptions<CrudAppContext> options) : base (options)
        {
        }

        // entities
        public DbSet<Person> Persons { get; set; }
        public DbSet<Phone> Phones { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Unique keys for Person
            builder.Entity<Person>().HasIndex( p => p.Cpf ).IsUnique();
            builder.Entity<Person>().HasIndex( p => p.Email ).IsUnique();

            // Unique double key for Phone
            builder.Entity<Phone>().HasIndex( p => new{p.Ddd, p.Numero} ).IsUnique();

            // Defining person -> phone relationship
            builder.Entity<Person>().HasMany( p => p.Phones );
        }
    }
}