export class Phone {
    constructor(
        public id: number, 
        public personId: number, 
        public ddd: number, 
        public numero: number, 
    ) {}
}
