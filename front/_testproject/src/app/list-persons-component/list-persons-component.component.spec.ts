import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPersonsComponentComponent } from './list-persons-component.component';

describe('ListPersonsComponentComponent', () => {
  let component: ListPersonsComponentComponent;
  let fixture: ComponentFixture<ListPersonsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPersonsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPersonsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
