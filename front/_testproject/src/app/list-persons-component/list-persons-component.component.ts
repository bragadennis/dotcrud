import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';


@Component({
  selector: 'app-list-persons-component',
  templateUrl: './list-persons-component.component.html',
  styleUrls: ['./list-persons-component.component.css']
})

export class ListPersonsComponentComponent implements OnInit {
  private persons: Array<object> = [];

  constructor(private apiService: APIService) { }

  ngOnInit() {
    this.getPersons();
  }

  public getPersons()
  {
    this.apiService.listPersons().subscribe((data: Array<object>) => {
      this.persons = data;

      console.log( data );
    });
  }

  public getAge(person)
  {
    return 2018 - new Date( person.dataNascimento ).getFullYear();
  }
}
