import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPhonesComponentComponent } from './list-phones-component.component';

describe('ListPhonesComponentComponent', () => {
  let component: ListPhonesComponentComponent;
  let fixture: ComponentFixture<ListPhonesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPhonesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPhonesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
