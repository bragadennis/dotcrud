import {Phone} from './phone';

export class Person {
    constructor(
        public id: number, 
        public name: string, 
        public cpf: string,
        public email: string, 
        public dataNascimento: string, 
        public phones?: Array<Phone>, 
    ){ }
}
