import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ListPersonsComponentComponent } from './list-persons-component/list-persons-component.component';
import { CreatePersonsComponentComponent } from './create-persons-component/create-persons-component.component';
import { ListPhonesComponentComponent } from './list-phones-component/list-phones-component.component';
import { CreatePhonesComponentComponent } from './create-phones-component/create-phones-component.component';
import { SearchPhonesComponentComponent } from './search-phones-component/search-phones-component.component';
import { SearchPersonsComponentComponent } from './search-persons-component/search-persons-component.component';
import { AppRoutingModuleModule } from './app-routing-module/app-routing-module.module';
import { UpdatePersonsComponentComponent } from './update-persons-component/update-persons-component.component';

@NgModule({
  declarations: [
    AppComponent,
    ListPersonsComponentComponent,
    CreatePersonsComponentComponent,
    ListPhonesComponentComponent,
    CreatePhonesComponentComponent,
    SearchPhonesComponentComponent,
    SearchPersonsComponentComponent,
    UpdatePersonsComponentComponent
  ],
  imports: [
    BrowserModule, 
    HttpClientModule, 
    AppRoutingModuleModule, 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
