import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';
import { Person } from '../person';

@Component({
  selector: 'app-create-persons-component',
  templateUrl: './create-persons-component.component.html',
  styleUrls: ['./create-persons-component.component.css']
})
export class CreatePersonsComponentComponent implements OnInit {
  constructor(private apiService: APIService) { }

  ngOnInit() {}

  public createPerson()
  {
    let person = new Person (
      null, 
      "Chico Antôim", 
      "123.123.123-12",
      "08-31-1990", 
      "asfd@asdf.com", 
    );

    this.apiService.createPerson( person ).subscribe((response) => {
      console.log( response );
    });
  }
}
