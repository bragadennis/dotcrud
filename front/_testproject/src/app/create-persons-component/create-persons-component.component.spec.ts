import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePersonsComponentComponent } from './create-persons-component.component';

describe('CreatePersonsComponentComponent', () => {
  let component: CreatePersonsComponentComponent;
  let fixture: ComponentFixture<CreatePersonsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePersonsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePersonsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
