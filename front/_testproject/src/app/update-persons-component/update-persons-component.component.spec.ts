import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePersonsComponentComponent } from './update-persons-component.component';

describe('UpdatePersonsComponentComponent', () => {
  let component: UpdatePersonsComponentComponent;
  let fixture: ComponentFixture<UpdatePersonsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePersonsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePersonsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
