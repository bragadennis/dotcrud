import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPhonesComponentComponent } from './search-phones-component.component';

describe('SearchPhonesComponentComponent', () => {
  let component: SearchPhonesComponentComponent;
  let fixture: ComponentFixture<SearchPhonesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPhonesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPhonesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
