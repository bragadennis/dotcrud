import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import { ListPersonsComponentComponent } from '../list-persons-component/list-persons-component.component';
import { CreatePersonsComponentComponent } from '../create-persons-component/create-persons-component.component';
import { UpdatePersonsComponentComponent } from '../update-persons-component/update-persons-component.component';

const routes: Routes = [
  {path: '', redirectTo: 'person', pathMatch: 'full'}, 
  {
    path: 'person', 
    component: ListPersonsComponentComponent
  }, 
  {
    path: 'person/create', 
    component: CreatePersonsComponentComponent
  }, 
  {
    path: 'person/update', 
    component: UpdatePersonsComponentComponent
  }
];

@NgModule({
  imports: [
    CommonModule, 
    RouterModule.forRoot(routes), 
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModuleModule { }
