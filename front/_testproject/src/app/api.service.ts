import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class APIService { 
  API_URL  = 'http://localhost:5000';   // Port served by .NET
  BASE_URL = this.API_URL + "/api/v1";  // API versioning

  constructor(private httpClient: HttpClient) { }

  // Person requests
  listPersons() {
    return this.httpClient.get(this.BASE_URL + '/person');
  }

  getPerson( person_id ) {
    return this.httpClient.get( this.BASE_URL + '/person/' + person_id);
  }

  createPerson( person ) {
    return this.httpClient.post(`${this.BASE_URL}/person`, person);
  }

  updatePerson( person_id, person ) {
    return this.httpClient.put(this.BASE_URL + '/person/' + person_id, person);
  }

  deletePerson( person_id )  {
    return this.httpClient.delete(this.BASE_URL + '/person/' + person_id);
  }

  // Phones requests
  listPhones(person_id) {
    return this.httpClient.get(this.BASE_URL + '/person/' + person_id + '/phone');
  }

  getPhone( person_id, phone_id ) {
    return this.httpClient.get( this.BASE_URL + '/person/' + person_id + '/phone/' + phone_id);
  }

  createPhone( person_id, phone ) {
    return this.httpClient.post(this.BASE_URL + '/person/' + person_id + '/phone', phone);
  }

  updatePhone( person_id, phone_id, phone ) {
    return this.httpClient.put(this.BASE_URL + '/person/' + person_id + '/phone/' + phone_id, phone);
  }

  deletePhone( person_id, phone_id )  {
    return this.httpClient.delete(this.BASE_URL + '/person/' + person_id + '/phone/' + phone_id);
  }
}
