import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePhonesComponentComponent } from './create-phones-component.component';

describe('CreatePhonesComponentComponent', () => {
  let component: CreatePhonesComponentComponent;
  let fixture: ComponentFixture<CreatePhonesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePhonesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePhonesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
