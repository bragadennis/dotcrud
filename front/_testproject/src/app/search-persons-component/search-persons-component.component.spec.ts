import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPersonsComponentComponent } from './search-persons-component.component';

describe('SearchPersonsComponentComponent', () => {
  let component: SearchPersonsComponentComponent;
  let fixture: ComponentFixture<SearchPersonsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPersonsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPersonsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
